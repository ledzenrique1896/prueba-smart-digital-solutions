﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
    public class Producto
    {
        public int ID { get; set; }
        public String Descripcion { get; set; }
        public Decimal Precio { get; set; }
        public Boolean Estado { get; set; }
    }
}
