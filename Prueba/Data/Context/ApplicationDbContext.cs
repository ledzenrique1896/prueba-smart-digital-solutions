﻿using Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Data.Context
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Valores por Defecto   
            modelBuilder.Entity<Producto>(entity =>
            {
                entity.HasKey(e => e.ID).HasName("PrimaryKey_IdProducto");
                entity.Property(e => e.Descripcion).HasMaxLength(150).IsUnicode(false).IsRequired();
                entity.Property(e => e.Precio).HasColumnType("decimal(19,4)").IsUnicode(false).IsRequired();
            });
            base.OnModelCreating(modelBuilder);
        }


        public override int SaveChanges()
        {
            SetModifiedInformation();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SetModifiedInformation();
            int rtn = int.MinValue;

            try
            {
                rtn = await base.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException)
            {
                SetModifiedInformation();
            }

            return rtn;
        }

        public void SetModifiedInformation()
        {
        }

        public virtual DbSet<Producto> Producto { get; set; }

    }
}
