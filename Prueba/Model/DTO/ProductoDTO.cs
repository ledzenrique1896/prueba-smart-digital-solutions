﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.DTO
{
    public class ProductoCreacionDTO
    {
        public String Descripcion { get; set; }
        public Decimal Precio { get; set; }
        public Boolean Estado { get; set; }

    }
    public class ProductoDTO : ProductoCreacionDTO
    {
        public int ID { get; set; }
    }
    public class ProductoActualizacionDTO : ProductoCreacionDTO
    {
        public int ID { get; set; }
    }
}
