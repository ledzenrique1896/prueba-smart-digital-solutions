﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prueba.Models
{
    public class Producto
    {
        public String ID { get; set; }
        public String Descripcion { get; set; }
        public Decimal Precio { get; set; }
        public Boolean Estado { get; set; }
    }
}
