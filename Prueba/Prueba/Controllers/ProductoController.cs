﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contract;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model.DTO;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace Parte1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private IMapper _mapper;
        private IProductoService _service;

        public ProductoController(IMapper mapper, IProductoService service)
        {
            _mapper = mapper;
            _service = service;
        }

        //get
        [HttpGet(Name = "ObtenerProductos")]
        public async Task<ActionResult<IEnumerable<ProductoDTO>>> GetAllProducto()
        {
            try
            {
                var entities = await _service.GetAllProducto();

                var rtn = _mapper.Map<List<ProductoDTO>>(entities);
                return rtn;
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        //get
        [HttpGet("{IdProducto}", Name = "ObtenerProductoPorId")]
        public async Task<ActionResult<ProductoDTO>> GetProductoById(int IdProducto)
        {
            try
            {
                var Producto = await _service.GetProductoById(IdProducto);

                if (Producto == null)
                {
                    return NotFound();
                }
                else
                {
                    var ProductoDTO = _mapper.Map<ProductoDTO>(Producto);
                    return ProductoDTO;
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        //post
        [HttpPost(Name = "CrearProducto")]
        public async Task<ActionResult> CrearProducto([FromBody] ProductoCreacionDTO ProductoCreacionDTO)
        {
            try
            {
                var Producto = _mapper.Map<Producto>(ProductoCreacionDTO);

                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }
                await _service.AddProducto(Producto);
                var ProductoDTO = _mapper.Map<ProductoDTO>(Producto);
                return new CreatedAtRouteResult("ObtenerProductoPorId", new { IdProducto = Producto.ID }, ProductoDTO);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }

        //put
        [HttpPut("{IdProducto}", Name = "ActualizarProducto")]
        public async Task<ActionResult> UpdateProducto(int IdProducto, [FromBody] Producto producto)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid model object");
                }

                var Producto = _mapper.Map<Producto>(producto);
                var ProductoToUpdate = await _service.GetProductoById(IdProducto);

                if (ProductoToUpdate == null)
                {
                    return NotFound();
                }

                await _service.UpdateProducto(ProductoToUpdate, Producto);
                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }

        }

        //delete
        [HttpDelete("{IdProducto}", Name = "EliminarProducto")]
        public async Task<ActionResult<Producto>> DeleteProducto(int IdProducto)
        {
            try
            {
                var Producto = await _service.GetProductoById(IdProducto);
                if (Producto == null)
                {
                    return NotFound();
                }

                await _service.DeleteProducto(Producto);
                return NoContent();
            }
            catch (DbUpdateException ex)
            {
                SqlException sqlException = ex.GetBaseException() as SqlException;
                if (sqlException != null)
                {
                    var number = sqlException.Number;
                    if (number == 547)
                        return StatusCode(500, "No es posible eliminar el elemento porque contiene dependencias.");
                    else
                        return StatusCode(500, "Internal server error");
                }
                else
                    return StatusCode(500, "Internal server error");
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
