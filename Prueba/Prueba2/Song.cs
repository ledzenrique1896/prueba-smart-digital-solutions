﻿using System;
using System.Collections.Generic;

namespace Parte2
{
    public class Song
    {
        /// <summary>
        /// Una lista de reproducción se considera una lista de reproducción repetida 
        /// si alguna de las canciones contiene una referencia a una canción anterior en la lista de reproducción. 
        /// De lo contrario, la lista de reproducción terminará con la última canción que apunta a null. 
        /// Implemente una función IsRepeatingPlaylist que de manera eficiente con respecto al tiempo utilizado, 
        /// devuelva verdadero si una lista de reproducción se repite o falso si no se repite.
        /// 
        /// Por ejemplo, el siguiente código se imprime verdadero como ambas canciones entre sí:
        /// 
        /// Song first = new Song("Hello")
        /// Song second = new Song("Eye of the Tiger");
        /// 
        /// first.NextSong = second;
        /// second.NextSong = first;
        /// Console.WriteLine(first.IsRepeatingPlaylist());
        /// 
        /// </summary>

        private string name;
        public Song NextSong { get; set; }

        public Song(string name)
        {
            this.name = name;
        }

        public bool IsRepeatingPlaylist()
        {
            return (NextSong.NextSong.Equals(this));
        }

        static void Main(string[] args)
        {
            Song first = new Song("Hello");
            Song second = new Song("Eye of the tiger");

            first.NextSong = second;
            second.NextSong = first;

            Console.WriteLine(first.IsRepeatingPlaylist());
            Console.ReadKey();
        }

        public override bool Equals(object obj)
        {
            var song = obj as Song;
            return song != null &&
                   name.ToLower().Replace(" ", "") == song.name.ToLower().Replace(" ", "");
        }
    }
}
