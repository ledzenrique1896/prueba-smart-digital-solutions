﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contract
{
    public interface IProductoService : IServicioBase<Producto>
    {
        Task<IEnumerable<Producto>> GetAllProducto();
        Task<Producto> GetProductoById(int IdProducto);
        Task AddProducto(Producto Producto);
        Task UpdateProducto(Producto ProductoToUpdate, Producto Producto);
        Task DeleteProducto(Producto Producto);
    }
}
