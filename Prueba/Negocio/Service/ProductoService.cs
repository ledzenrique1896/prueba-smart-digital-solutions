﻿using Contract;
using Data.Context;
using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Service
{
    public class ProductoService : ServicioBase<Producto>, IProductoService
    {
        public ProductoService(ApplicationDbContext servicioContext) : base(servicioContext)
        {
        }

        public async Task AddProducto(Producto Producto)
        {
            Create(Producto);
            await SaveAsync();
        }

        public async Task DeleteProducto(Producto Producto)
        {
            Delete(Producto);
            await SaveAsync();
        }

        public async Task<IEnumerable<Producto>> GetAllProducto()
        {
            return await FindAll()
               .OrderByDescending(x => x.ID)
               .ToListAsync();
        }

        public async Task<Producto> GetProductoById(int IdProducto)
        {
            return await FindByCondition(x => x.ID == IdProducto).SingleAsync();
        }

        public async Task UpdateProducto(Producto ProductoToUpdate, Producto Producto)
        {
            ProductoToUpdate.Descripcion = Producto.Descripcion;
            ProductoToUpdate.Precio = Producto.Precio;
            ProductoToUpdate.Estado = Producto.Estado;
            Update(ProductoToUpdate);
            await SaveAsync();
        }

    }
}
