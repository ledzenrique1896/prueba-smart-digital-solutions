﻿using Contract;
using Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Service
{
    public abstract class ServicioBase<T> : IServicioBase<T> where T : class
    {
        protected ApplicationDbContext ServicioContext { get; set; }

        public ServicioBase(ApplicationDbContext servicioContext)
        {
            this.ServicioContext = servicioContext;
        }

        public IQueryable<T> FindAll()
        {
            return this.ServicioContext.Set<T>();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            if (expression != null)
                return this.ServicioContext.Set<T>()
                    .Where(expression);
            else
                return this.ServicioContext.Set<T>();
        }

        public void Create(T entity)
        {
            this.ServicioContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.ServicioContext.Entry(entity).State = EntityState.Modified;
            this.ServicioContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.ServicioContext.Set<T>().Remove(entity);
        }

        public async Task SaveAsync()
        {
            await this.ServicioContext.SaveChangesAsync();
        }

        public int getCount(T entity)
        {
            return this.ServicioContext.Set<T>().Count();
        }
    }
}
